import asyncio
import requests
from flask import Flask, request, render_template
app = Flask(__name__)

@app.route('/')
def home():
  return render_template('demo.html')

@app.route('/get_hotels_list', methods=['POST'])
def get_hotels_list():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    test = loop.run_until_complete(parallel_hotels_request(loop))
    return test

async def parallel_hotels_request(loop):
    post_data = {
      'city' :  request.form['city'],
      'checkin' : request.form['checkin'],
      'checkout' : request.form['checkout']
    }

    futures = []
    for provider in ['snaptravel', 'retail']:
        futures.append(loop.run_in_executor(None, get_hotels_request, post_data, provider))

    snaptravel_hotels = {}
    hotelscom_hotels = {}
    for response, provider in await asyncio.gather(*futures):
        if provider == 'snaptravel':
            snaptravel_hotels = response
        else:
            hotelscom_hotels = response

    hotels = hotels_set(snaptravel_hotels, hotelscom_hotels)

    return render_template('table.html', data=hotels)

def get_hotels_request(post_data, provider):
    post_data['provider'] = provider
    response = requests.post('https://experimentation.snaptravel.com/interview/hotels', json=post_data)
    return response.json(), provider 

# take two hotel hashes and return the intersect of both
# better implementation would be storing their ids as keys
# and the rest of their data as the value to get O(n) time complexity
def hotels_set(s_hotels, h_hotels):
    hotels = []
    for s_hotel in s_hotels['hotels']:
        for h_hotel in h_hotels['hotels']:
            if s_hotel['id'] == h_hotel['id']:
                s_hotel['hotelscom_price'] = h_hotel['price']
                s_hotel['snaptravel_price'] = s_hotel['price']
                hotels.append(s_hotel)

    return hotels
